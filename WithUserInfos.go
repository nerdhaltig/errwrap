package errwrap

type ErrorWithUserInfos interface {
	Tenant() string
	UserName() string

	Error() string
	Unwrap() error
}

type errUserInfos struct {
	ErrTenant   string `json:"tenant"`
	ErrUserName string `json:"username"`

	Err error `json:"-"` // The original error. Same reason as above.
}

func WithUserInfos(err error, tenant string, username string) error {
	if err == nil {
		return nil
	}

	return errUserInfos{
		ErrTenant:   tenant,
		ErrUserName: username,
		Err:         err,
	}
}

func (err errUserInfos) Tenant() string {
	return err.ErrTenant
}

func (err errUserInfos) UserName() string {
	return err.ErrUserName
}

func (err errUserInfos) Error() string {
	if err.Err != nil {
		return err.Err.Error()
	}
	return ""
}

func (err errUserInfos) Unwrap() error {
	return err.Err // Returns inner error
}
