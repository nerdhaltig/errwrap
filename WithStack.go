package errwrap

import (
	"errors"
	"runtime"
)

type ErrorWithStack interface {
	Stack() string

	Error() string
	Unwrap() error
}

type errStack struct {
	ErrStackBuffer []byte `json:"stack"`
	Err            error  `json:"-"` // The original error. Same reason as above.
}

// WithStack add stack information to an err.
// If there are already stack-infos, this function does nothing and keep the original stack-infos
//
// You can get the Stack with:
//
//	 var errWithStack errwrap.ErrorWithStack
//	 if errors.As(err, &errWithStack) {
//			errWithStack.Stack()
//	 }
func WithStack(err error) error {
	if err == nil {
		return nil
	}

	// check if err already contains the stack
	var curErrStack errStack
	if errors.As(err, &curErrStack) {
		return err
	}

	newErr := errStack{
		ErrStackBuffer: make([]byte, 2048),
		Err:            err,
	}

	// fill it with stack
	runtime.Stack(newErr.ErrStackBuffer, false)

	return newErr
}

func (err errStack) Stack() string {
	return string(err.ErrStackBuffer)
}

func (err errStack) Error() (errorText string) {
	if err.Err != nil {
		return err.Err.Error()
	}
	return ""
}

func (err errStack) Unwrap() error {
	return err.Err // Returns inner error
}
