package errwrap_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/errwrap"
)

type unknown struct{}

func (unknown) Error() string {
	return ""
}

func TestPackedError(t *testing.T) {

	// tag::usage[]
	// pack an error together
	err := errors.New("Something goes wrong")
	err = errwrap.WithStack(err)
	err = errwrap.WithState(err, 500)
	err = errwrap.WithUserInfos(err, "local", "admin")
	// end::usage[]

	// tag::usage[]

	// extract the stack info from the error
	var stackTrace string
	var errWithStack errwrap.ErrorWithStack
	if errors.As(err, &errWithStack) {
		stackTrace = errWithStack.Stack()
	}
	// end::usage[]
	assert.NotEmpty(t, stackTrace)
	assert.Equal(t, "Something goes wrong", errWithStack.Error())

	// tag::usage[]

	// extract status from the error
	var errState int
	var errWithStatus errwrap.ErrorWithState
	if errors.As(err, &errWithStatus) {
		errState = errWithStatus.State()
	}
	// end::usage[]
	assert.Equal(t, 500, errState)
	assert.Equal(t, "Something goes wrong", errWithStatus.Error())

	// tag::usage[]

	// extract userinfos from the error
	var errWithUserInfo errwrap.ErrorWithUserInfos
	var Tenant, Username string
	if errors.As(err, &errWithUserInfo) {
		Tenant = errWithUserInfo.Tenant()
		Username = errWithUserInfo.UserName()
	}
	// end::usage[]
	assert.True(t, errors.As(err, &errWithUserInfo))
	assert.Equal(t, "local", Tenant)
	assert.Equal(t, "admin", Username)
	assert.Equal(t, "Something goes wrong", errWithUserInfo.Error())

	// unwrap to unknown :)
	var errUnknown unknown
	assert.False(t, errors.As(err, &errUnknown))

	// and finally the error ?
	assert.Equal(t, "Something goes wrong", err.Error())

}

func TestNoError(t *testing.T) {

	// pack an error together
	err := errwrap.WithStack(nil)
	err = errwrap.WithState(err, 500)
	err = errwrap.WithUserInfos(err, "local", "admin")

	// With Stack
	var errWithStack errwrap.ErrorWithStack
	assert.False(t, errors.As(err, &errWithStack))
	assert.Nil(t, errWithStack)

	// With status
	var errWithStatus errwrap.ErrorWithState
	assert.False(t, errors.As(err, &errWithStatus))
	assert.Nil(t, errWithStatus)

	// With userinfo
	var errWithUserInfo errwrap.ErrorWithUserInfos
	assert.False(t, errors.As(err, &errWithUserInfo))
	assert.Nil(t, errWithUserInfo)

	assert.Nil(t, err)
}

func TestPanic(t *testing.T) {

	err := errors.New("Something goes wrong")

	// pack an error together
	assert.Panics(t, func() { errwrap.PanicOnError(err) })
}

func BenchmarkErrorChecking(b *testing.B) {

	var errorFromAfunction error = nil

	b.Run("before with if", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			if errorFromAfunction != nil {
				err := errwrap.WithStack(errorFromAfunction)
				err = errwrap.WithState(err, 500)
				err = errwrap.WithUserInfos(err, "local", "admin")
				_ = err
			}
		}
	})

	b.Run("without if", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			err := errwrap.WithStack(errorFromAfunction)
			err = errwrap.WithState(err, 500)
			err = errwrap.WithUserInfos(err, "local", "admin")
			_ = err
		}
	})

}
